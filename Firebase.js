import firebase from 'firebase';
import axios from 'axios';
import { Toast } from "native-base";

const PUSH_ENDPOINT = 'http://172.31.12.143:3000/push';
const LIMIT = 20

class Fire {
  constructor() {
    this.init();
    this.observeAuth();
  }

  init = () => {
    if (firebase && !firebase.apps.length) {
      firebase.initializeApp({
        apiKey: "AIzaSyBYjI2hisITqxcQXxw_eFRajAXb1feeNZo",
        authDomain: "gether-78c7e.firebaseapp.com",
        databaseURL: "https://gether-78c7e.firebaseio.com",
        projectId: "gether-78c7e",
        storageBucket: "gether-78c7e.appspot.com",
        messagingSenderId: "587982677381"
      });
    }
  };

  observeAuth = () =>
    firebase.auth().onAuthStateChanged(this.onAuthStateChanged);

  onAuthStateChanged = user => {
    if (!user) {
      Toast.show({
        text: "Need to sign in",
        buttonText: "Okay",
        type: "warning"
      });
    } else {
      Toast.show({
        text: "Great to see you",
        buttonText: "Okay",
        type: "success"
      });
    }
  };


  get uid() {
    return (firebase.auth().currentUser || {}).uid;
  }

  get email() {
    return (firebase.auth().currentUser || {}).email;
  }

  get avatar() {
    return firebase.auth().currentUser.photoURL
  }

  get ref() {
    return firebase.database().ref('messages');
  }

  get ref_public() {
    return firebase.database().ref('Messages/PublicChat');
  }

  get ref_chat_room() {
    return firebase.database().ref('ChatRoom/Private');
  }

  get ref_user_chat(){
    return firebase.database().ref('UserChat');
  }

  get ref_private_user_chat(){
    return firebase.database().ref('UserChat/Private');
  }

  get ref_message(){
    return firebase.database().ref('Messages/Private');
  }

  get ref_user() {
    return firebase.database().ref('/devices');
  }

  parse = snapshot => {
    const { timestamp: numberStamp, text, user } = snapshot.val();
    const { key: _id } = snapshot;
    const timestamp = new Date(numberStamp);
    const message = {
      _id,
      timestamp,
      text,
      user,
    };
    return message;
  };

  async currentUser() {
    const user = await firebase.auth().currentUser;
    return user;
  }

  on = (callback, flag, chatID ) => {
    if(flag == 'public'){
      this.ref_public
        .limitToLast(20)
        .on('child_added', snapshot => callback(this.parsePublicDataFromMessage(snapshot.val())));
    }else{
      this.ref_message.child(chatID)
        .limitToLast(20)
        .on('child_added', snapshot => {
          callback(this.parseDataFromMessage(snapshot.val()))
        });
    }
  }

  page = callback =>
    this.ref_message
      .on('child_added', snapshot => callback(this.parseDataFromMessage(snapshot.val())));

  get timestamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }

  // send the message to the Backend
  send = (messages, chatID) => {
    message = messages[0]
    this.createMessageTextData(message, chatID)
    this.pushNotify(this.email, message.text)
  };

  sendPublic = (messages, chatID) => {
    message = messages[0]
    this.createPublicMessageTextData(message, chatID)
    this.pushNotify(this.email, message.text)
  };

  // save IdDeviceToken
  pushNotify = (email, message) => {
    console.log('pash', email, message)
    firebase.database().ref('devices').once('value').then(function(snap){
      let result = [];
      for(let k in snap.val()) {
        result.push(snap.val()[k]['expoDeviceToken'])
      }
      console.log('devices',result)
      return axios({
        method: 'post',
        url: PUSH_ENDPOINT,
        data: { data: { username: email, message: message, token: result } }
      })
    })
  };

  // Chat: { chatID1: {}, chatID2: {} }
  createPrivateChatData(toUserID) {
    data = {
      members: {},
      lastMessageSent: '',
      type: 'private'
    }
    // Key = fromUserID + toUserID

    if(this.uid <= toUserID){
      chatID = this.uid + toUserID
    }else{
      chatID = toUserID + this.uid
    }

    // Check key exits
    this.ref_user_chat.child(this.uid).child(chatID).once('value', (snapshot) => {
      if(snapshot.val()){
      }else{
        this.ref_chat_room.child(chatID).set(data)
        this.createPrivateChatUserData(this.uid, chatID)
      }
    })

    return chatID
  }

  // @@@@@
  // Messages: { chatId: { messageId: { .... }}}
  createMessageTextData(message, chatID) {
    console.log(chatID)
    data = {
      chatId: chatID,
      message: message.text,
      messageId: message._id,
      messageType: 'text',
      senderId: message.user._id,
      timestamp: '1233444'
    }
    this.ref_message.child(chatID).child(message._id).set(data)
  }

  createPublicMessageTextData(message, chatID) {
    data = {
      chatId: chatID,
      message: message.text,
      messageId: message._id,
      messageType: 'text',
      senderId: message.user._id,
      timestamp: '1233444'
    }
    this.ref_public.child(message._id).set(data)
  }


  // @@@@@
  // UserChat =>
  // userId = {
  //   chatId1: true,
  //   chatId2: true
  // }
  createPrivateChatUserData(userID, chatID) {
    this.ref_private_user_chat.child(userID).child(chatID).set(true)
  }

  parseDataFromMessage(snapData) {
    messParse = snapData

    console.log('parse ------', snapData)
    user = {
      _id: messParse.senderId,
      name: 'sdsd',
      avatar: ''
    }
    message = {
      _id: messParse.messageId,
      timestamp: new Date(messParse.timestamp),
      text: messParse.message,
      user,
    };
    return message;
  }

  parsePublicDataFromMessage(snapData){
    messParse = snapData
    user = {
      _id: messParse.senderId,
      name: 'sdsd',
      avatar: ''
    }
    message = {
      _id: messParse.messageId,
      timestamp: new Date(messParse.timestamp),
      text: messParse.message,
      user,
    };
    return message;
  }

  async getChatListOfCurrentUser(){
    let tada = await this.ref_user_chat.child(this.uid).on('value', (snapshot) => {
      return snapshot.val()
    })
  }

  append = message => this.ref_q.push(message);

  // close the connection to the Backend
  off() {
    this.ref.off();
  }
}

Fire.shared = new Fire();
export default Fire;
